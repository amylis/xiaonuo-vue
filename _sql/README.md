## sql相关说明
1. 【xiaonuo-vue-pub.sql】 为mysql版本的sql，如果您的数据库为mysql，可直接运行此sql
2. 【xiaonuo-vue-pub-oracle.sql】 为oracle版本的sql，如果您的数据库为oracle，可直接运行此sql
3. 【xiaonuo-vue-pub-mssql.sql】 为mssql版本的sql，如果您的数据库为mssql，可直接运行此sql